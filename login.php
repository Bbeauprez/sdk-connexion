<?php

use utils\provider;

require_once "utils/ProviderInterface.php";
require_once "utils/Provider.php";

$redirectUri = "&redirect_uri=http://localhost:8081/login.php?";

if ($_GET["provider"] != null) {

    $path = "utils/conf/" . $_GET["provider"] . ".json";
    if (file_exists($path)) {
        $provider = new Provider(json_decode(file_get_contents($path), true), $_GET["provider"]);


        $redirectUri .= "provider=" . $provider->getProviderName();
        if ($_GET["code"] == null) {
            header('Location: ' . $provider->getAuthorizationUrl() . $redirectUri);
        } else {
            $code = $_GET["code"];

            $output = file_get_contents($provider->getAccessTokenUrl() . $redirectUri . "&code=" . $code);
            if ($_GET["provider"] == "github"){
                $out = explode("&", $output)[0];
                $out = explode("=", $out)[1];
                $json = ["access_token"=>$out];
            }else{
                $json = json_decode($output, true);
            }

            print_r($json["access_token"]);
            echo $provider->getUsersInfo($json["access_token"]);
        }
    }else{
        echo "no conf file for this provider<br>";
    }
} else {
    echo "No provider renseigned";
}
